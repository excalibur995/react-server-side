import React from "react";
import { renderToString } from "react-dom/cjs/react-dom-server.browser.development";
import Home from "../components/Home";

export default () => {
  const content = renderToString(<Home />);
  return `
  <html>
  <head></head>
  <body>
      <div id="root">${content}</div>
      <script src="bundle.js"/>
  </body>
</html>
  `;
};
