import React from "react";
import Route from "./Routes";
import BrowserRouter from "react-router-dom/BrowserRouter";
import { hydrate } from "react-dom/cjs/react-dom.development";

hydrate(
  <BrowserRouter>
    <Route />
  </BrowserRouter>,
  document.querySelector("#root")
);
